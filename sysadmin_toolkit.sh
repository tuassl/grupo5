#!/bin/bash

#--------------------------------------------------------------------------------#
#                           Toolkit Sysadmin                                     #
#  Esta herramienta es un conjunto de scripts con un menú interactivo en shell.  #
#  Cada opcion del menu tiene submenus para ejecutar las tareas solicitadas.     #
#                                                                                #
#                    Universidad Nacional del Comahue                            #
#                         Facultad de Informática                                #
#                                Grupo 5                                         #
#                                                                                #
#                    Fernando Andrés Pinilla Álvarez                             #
#                                                                                #
#--------------------------------------------------------------------------------#

# Biblioteca de recursos para el funcionamiento del Toolkit
source biblioteca_toolkit.sh ;


################################# FUNCIONES DE GESTION DE USUARIOS #############################

#Submenú de gestion de usuarios con sus opciones e interaciones
function gestion_usuarios {
clear
echo -e " \n****************************************************************
	                 GESTIÓN DE USUARIOS
**************************************************************** "
echo ""
echo " 1) Alta de Usuarios  (Administrador)"
echo " 2) Cruce de Claves" 
echo " 3) Baja de Usuarios  (Administrador)"
echo " 4) Reporte de Usuarios Regulares"
echo " 5) Reporte de Usuarios del Sistema"
echo " 6) Volver al Menú Principal"
echo " 7) Ayuda"
echo " 8) Salir"
read -p " Seleccione una opción: " opci
echo ""
	case $opci in
	           1) alta_usuario 
                ;;
		        2) cruzar_llaves 
                ;;
		        3) baja_usuario 
                ;;
		        4) reporteReg
                ;;
		        5) reporteSys
                ;;
		        6) return 
                ;;
              7) clear
                 sed -n '164,+36p' biblioteca_toolkit.sh
                 read -n 1 -s -r -p "- Presione una tecla para continuar -"
                ;;
              8) echo -e " \nHasta la Próxima\n "; exit 0 
                ;;
		        *) echo -e " \n ERROR!!!, opción inválida, por favor elija entre las opciones disponibles. \n "
                 read -n 1 -s -r -p "- Presione una tecla para continuar -"
                 return
                ;;
	esac
}


#Funcion que recibe un listado de usuarios y los crea con home y pass aleatoria.
#genera un archivo con los usuarios creados y sus contraseñas
function listadoAlta {
read -p "Ingrese la ruta del archivo : " rutaLista;
if [[ -f $rutaLista ]]; then
           listaUsers=$(cut -d "," -f 1 "$rutaLista")
           for usrName in $listaUsers; 
                   do
                          existe_usr=$(verifica_user $usrName)
                          randomPassw=$(genera_pass);
                          if [[ "$existe_usr" -eq 0 ]] && /sbin/useradd -m -p $randomPassw "$usrName" > /dev/null 2>&1; 
                                then echo "Usuario $usrName creado con éxito!consulte user_creados.txt para más informacion"
                                     echo "Usuario:$usrName con Password:$randomPassw" >> user_creados.txt
                                     sleep 1 #Necesario para que varíe la pass
                                else echo "ERROR!! no se pudo crear el usuario $usrName"
                          fi
                  done
   else  echo -e "\n ERROR!! ruta o archivo inválidos"; exit 99;
 fi        
}

#Funcion que recibe un listado de usuarios y los elimina del sistema junto con el correo y directorio home.
function listadoBaja {
read -p "Ingrese la ruta del archivo : " rutaListaB;
if [[ -f $rutaListaB ]]; then
           listaUsersB=$(cut -d "," -f 1 "$rutaListaB")
           for usrNameB in $listaUsersB; 
                   do
                          if /sbin/userdel -r "$usrNameB" > /dev/null 2>&1; 
                                then echo "Usuario $usrNameB directorio Home y correo eliminados con éxito!"
                                else echo "ERROR!! no se pudo eliminar el usuario $usrNameB"
                          fi
                  done
   else  echo -e "\n ERROR!! ruta o archivo inválidos"; exit 99;
 fi        
}


#Funcion que comprueba permisos y el método de creacion de usuarios.
function alta_usuario {
 verifica_root
 read -p "Desea ingresar un archivo CSV con un listado de nombres?: [S / N] : " SoN;
  case $SoN in
              S|s) listadoAlta
                   ;;

              N|n) read -p "Ingrese el nombre del nuevo usuario: " usrNew;
                   existe_user=$(verifica_user $usrNew);
                   randomPass=$(genera_pass);    
                   if  [[ "$existe_user" -eq 0 ]] && /sbin/useradd -m -p $randomPass "$usrNew" > /dev/null 2>&1; then
                              echo "Usuario $usrNew creado con éxito! consulte user_creados.txt para más informacion"
                              echo "Usuario:$usrNew con Password:$randomPass" >> user_creados.txt
                         else echo "ERROR!! no se pudo crear el usuario $usrNew"
                   fi ;;
                  
                *) echo -e "\nOpción inválida";;             
  esac
   read -n 1 -s -r -p "- Presione una tecla para continuar -"
  return
}     

#Funcion que comprueba permisos y el método de eliminación de usuarios.
function baja_usuario {
verifica_root
 read -p "Desea ingresar un archivo CSV con un listado de nombres?: [S / N] : " SoN;
  case $SoN in
              S|s) listadoBaja
                   ;;
              N|n) read -p "Ingrese el nombre del usuario a eliminar: " usrDel;
                   existe_ussr=$(verifica_user $usrDel);
                   if [[ "$existe_ussr" -eq 0 ]] && /sbin/userdel -r "$usrDel" > /dev/null 2>&1; then
                              echo "Usuario $usrDel directorio Home y correo eliminados con éxito!"
                         else echo "ERROR!! no se pudo eliminar el usuario $usrDel"
                   fi 
                   ;;           
                *) echo -e "\nOpción inválida";;
  esac
        read -n 1 -s -r -p "- Presione una tecla para continuar -"
  return
}     



#Funcion que dado una lista de usuarios y una lista de maquinas hace un cruce de llaves ssh.
#en caso de no existir llave, se genera.
function cruzar_llaves {
read -p "Ingrese la ruta del archivo csv con el listado de nombres : " listaNombres;
if [[ -f $listaNombres ]]; then
             listaNombres=$(cut -d "," -f 1 "$listaNombres")
             read -ep "Ingrese la ruta del archivo csv con el listado de maquinas: " listaMaquinas;
             if [[ -f $listaMaquinas ]]; then
                         listaMaquinas=$(cut -d "," -f 1 "$listaMaquinas")
                         for nombreUsr in $listaNombres; do
                                  su $nombreUsr --session-command echo "\n------------------------------------------------"
                                                                  echo "Usuario: $nombreUsr "
                                                                  echo "Verificando llave..."
                                  if [[ -f /home/$nombreUsr/.ssh/id_rsa.pub ]]; then 
                                              echo "Llave encontrada"

                                   else       echo "No se encontró llave. Generando..."
                                              ssh-keygen; echo "Llave generada con éxito"
                                  fi


       

                                for ip in $listaMaquinas; do
                                               if ping -c 1 $ip > /dev/null ; then 
                                                  echo -e "\n------------------------------------------------"
                                                  echo "Copiando llave de $nombreUsr hacia la maquina $ip"
                                                     if ssh-copy-id $nombreUsr@$ip; then echo "Llave copiada con éxito"
                                                     else echo "Ocurrió un problema al copiar la llave hacia $ip"
                                                     fi
                                                 else
                                                 echo -e "\n No se pudo establecer conexión con el destino $ip" 
                                               fi
                                done     
                         done    
                 else echo -e "\n ERROR!! ruta o archivo inválidos"                     
                fi
 

fi 
read -n 1 -s -r -p "- Presione una tecla para continuar -"
return                 
}


#Genera un reporte de usuarios regulares  basado en el rango de UID
#Recibe un archivo con una lista o elabora reporte localmente
function reporteReg {
  echo "Desea ingresar un archivo csv con un listado de maquinas (S) / (N)  o realizar 
un reporte para el host local? : "
  read YoN
      case $YoN in 
                  s|S) read -p "Ingrese la ruta del archivo csv con el listado de maquinas : " listaMaq;
                       if [[ -f $listaMaq ]]; then
                          ussr=$(whoami);
                          listaMaq=$(cut -d "," -f 1 "$listaMaq")
                              for ip in $listaMaq; do
                               ssh $ussr@$ip "getent passwd {1000..2000} | cut -d: -f1" > /tmp/repoRusers
                               regUser=$( cat /tmp/repoRusers )
                               regUserN=$( cat /tmp/repoRusers | wc -l ) 
                               cartel_report reg;
                               echo -e "El número de usuarios regulares es de $regUserN y los usuarios son: \n$regUser\n"
                              done

                         else echo -e "\n ERROR!! ruta o archivo inválidos"
                       fi
                       ;;
                  n|N) getent passwd {1000..2000} | cut -d: -f1 > /tmp/repoRusers;
                               chmod -f 777 /tmp/repoRusers
                               regUser=$( cat /tmp/repoRusers )
                               regUserN=$( cat /tmp/repoRusers | wc -l ) 
                               cartel_report reg;
                               echo -e "El número de usuarios regulares es de $regUserN y los usuarios son: \n$regUser\n"
                  ;;
                  *) echo -e "\n ERROR!! Opcion inválida";;
esac 
rm -f /tmp/repoRusers
read -n 1 -s -r -p "- Presione una tecla para continuar -"
return
}

#Genera un reporte de usuarios del sistema basado en el rango de UID
#Recibe un archivo con una lista o elabora reporte localmente
function reporteSys {
  echo "Desea ingresar un archivo csv con un listado de maquinas (S) / (N)  o realizar 
un reporte para el host local? : "
  read YoN
      case $YoN in 
                  s|S) read -p "Ingrese la ruta del archivo csv con el listado de maquinas : " listaM;
                       if [[ -f $listaM ]]; then
                          ussr=$(whoami);
                          listaM=$(cut -d "," -f 1 "$listaM")
                              for ip in $listaM; do
                               ssh $ussr@$ip "getent passwd | grep -v '/home' | cut -d: -f1" > /tmp/repoSusers
                               sysUser=$( cat /tmp/repoSusers )
                               sysUserN=$( cat /tmp/repoSusers | wc -l ) 
                               cartel_report sys;
                               echo -e "El número de usuarios regulares es de $sysUserN y los usuarios son: \n$sysUser\n"
                              done

                         else echo -e "\n ERROR!! ruta o archivo inválidos"
                       fi
                       ;;
                  n|N) getent passwd | grep -v '/home' | cut -d: -f1 > /tmp/repoSusers;
                               chmod -f 777 /tmp/repoSusers
                               sysUser=$( cat /tmp/repoSusers )
                               sysUserN=$( cat /tmp/repoSusers | wc -l ) 
                               cartel_report sys;
                               echo -e "El número de usuarios del sistema es de $sysUserN y los usuarios son: \n$sysUser\n"
                  ;;
                  *) echo -e "\n ERROR!! Opcion inválida";;
esac 
rm -f /tmp/repoSusers
read -n 1 -s -r -p "- Presione una tecla para continuar -"
return
}



############################# FUNCIONES DE GESTION DE DISCO #############################

#Recibe una lista de direcciones,o se ingresa una manualmente,  se conecta por ssh 
#realiza un filtro de la salida del comando df y lo muestra en forma de reporte
function espacio_disco {
userName=$(whoami);
read -p "Desea ingresar un archivo CSV con un listado de direcciones? [S / N] : " usrOpt
  case $usrOpt in
              S|s) read -p "Por favor ingrese la ruta del archivo : " ruta_archivo
                   if  [[  -f $ruta_archivo ]]; then
                           listaIpes=$(cut -d "," -f 1 "$ruta_archivo")
                           for ip in $listaIpes; do
                                 ssh $userName@$ip  "df -hT /" > /tmp/espacioDisk
                                 cat /tmp/espacioDisk | tail -n +2 | while read fich tipo tama uso disp porUso montaje;
                                               do                                               
                                                 porcent=100
                                                 porUsoSP=$(echo "$porUso" | cut -d "%" -f1)
                                                 result=$(echo "$porcent $porUsoSP" | awk '{print $1 - $2}')
                                                 echo "-------------------------------------"
                                                 echo "Reporte para el equipo con IP: $ip "
	            	                               echo "Partición: $fich"
	            	                               echo "Tipo de sistema de archivos: $tipo"
                                                 echo "Tamaño total: $tama"
	                                              echo "Espacio utilizado: $uso o $porUso"
                                                 echo "Disponible: $disp o $result% "
            		                               echo "Punto de montaje: $montaje"
                                                 echo "-------------------------------------"
                                              done             
                                      done  
                                                                
                   else echo -e "\n $ruta_archivo ERROR!! ruta o archivo inválidos"; exit 99; 
                   
                   fi ;;
                 
              N|n) read -p "Ha elegido conexion manual, ingrese la IP: " IP ;
                    ssh $userName@$IP "df -hT / | tail -n +2" | while read fich tipo tama uso disp porUso montaje
                                       do
                                         porcent=100
                                         porUsoSP=$(echo "$porUso" | cut -d "%" -f1)
                                         result=$(echo "$porcent $porUsoSP" | awk '{print $1 - $2}')
                                         echo "-------------------------------------"
                                         echo "Reporte para el equipo con IP: $IP "
	            	                       echo "Partición: $fich"
	            	                       echo "Tipo de sistema de archivos: $tipo"
                                         echo "Tamaño total: $tama"
	                                      echo "Espacio utilizado: $uso o $porUso"
                                         echo "Disponible: $disp o $result% "
            		                       echo "Punto de montaje: $montaje"
                                         echo "-------------------------------------"   
                                      done
                           if [[ $? -ne 0 ]] ; then          
                                echo "Ocurrió un error al conectar al host"
                           fi ;;    

                *) echo -e "\nOpción inválida" ;;
   esac
   read -n 1 -s -r -p "- Presione una tecla para continuar -"
   return
}


#Elabora un informe de rendimiento de los discos locales.
#mpstat es un complemento del paquete sysstat

function repo_rend_local() {
    verifica_root;
 if verifica_pkg mpstat; then    
    verifica_pkg iostat;
    cartel_report dl;
    iostat -phk sda; 
    read -n 1 -s -r -p "Presione una tecla para reporte en tiempo real "
    iotop;
 fi
}


#Elabora un reporte de la actividad de entrada y salida para una lista de equipos remotos 
#desactiva el modo interactivo, muestra solo procesos de i/o activos con 5 capturas por equipo.
function reporte_io_rem() {
verifica_root
ussrNam=$(whoami)
read -p "Ingrese la ruta del archivo csv con la lista de direcciones: " ruDir
if [[ -f $ruDir ]]; then
                 listDir=$(cut -d "," -f 1 "$ruDir")
                 for ip in $listDir; 
                      do                                                                             
                      ssh $ussrNam@$ip "[[ -f /usr/sbin/iotop ]]" 
                          vaSal=$?
                          if [[ $vaSal == 0 ]];then
                              cartel_report io;
                              iotop -bon 5;
                          
                          else read -p "No se encuentra el paquete solicitado, desea instalarlo? [ S / N ]" inst
                                case $inst in 
                                              s|S) apt install iotop
                                               ;;
                                              n|N) echo -e "\n Ha elegido no instalar el paquete."
                                               ;;
                                                *) echo -e "\n Opcion invalida"
                                               ;;
                                 esac
                         fi
                     done
                      
                                      
 else echo -e "\n $ruDir ERROR!! ruta o archivo inválidos"; exit 99; 

fi
}


#Funcion con el menú de gestion de discos.
function gestion_discos {
clear
echo -e " \n****************************************************************
	                 GESTIÓN DE DISCOS
**************************************************************** "
echo ""
echo " 1) Reporte de Espacio en Disco"
echo " 2) Reporte de Rendimiento de Discos Locales"
echo " 3) Reporte de Actividad de I/O (Entrada/Salida) en Equipos Remotos"
echo " 4) Volver al menú principal"
echo " 5) Ayuda"
echo " 6) Salir"
read -p " Seleccione una opción: " opt
echo ""
	case $opt in
                1) espacio_disco
                ;;
                2) repo_rend_local
                ;;
                3) reporte_io_rem
                ;;                       
                4) return 
                ;;
                5) clear 
                   sed -n '199,+28p' biblioteca_toolkit.sh
                   read -n 1 -s -r -p "- Presione una tecla para continuar -"
                ;;
                6) echo -e " \nHasta la Próxima\n "; exit 0 
                ;;
                *) echo -e " \n ERROR!!!, opción inválida, por favor elija entre las opciones disponibles. \n "
                   read -n 1 -s -r -p "- Presione una tecla para continuar -"
                   return
                ;;  
	esac
return
}


############################# FUNCIONES DE GESTION DE PROCESOS #############################

#Menu principal de Gestion de Procesos
function gestion_procesos {
    clear
	 echo -e " \n****************************************************************
	                 GESTIÓN DE PROCESOS
**************************************************************** "
echo ""
echo " 1) Reporte de los Primeros 5 Procesos por Consumo de Memoria"
echo " 2) Alerta de Cantidad de Procesos"
echo " 3) Monitorear un Proceso Especifico"
echo " 4) Eliminar un Proceso Específico"
echo " 5) Eliminar un Listado de Procesos "
echo " 6) Volver al Menú Principal"
echo " 7) Ayuda"
echo " 8) Salir"
echo ""
read -p " Seleccione una opcion: " optn
echo ""

case $optn in
     1) clear
        reporte_cinco
        read -n 1 -s -r -p "- Presione una tecla para continuar -"
        ;;
     2) alerta_proceso
        ;;
     3) monitorPS
        ;;
     4) PSkiller
        ;;
     5) serialPSkiller
        ;;
     6) return
        ;;
     7) clear
        sed -n '226,+37p' biblioteca_toolkit.sh 
        read -n 1 -s -r -p "- Presione una tecla para continuar -"
        ;;
     8) echo -e " \nHasta la Próxima\n "; exit 0 
        ;;
     *) echo -e " \n ERROR!!!, opción inválida, por favor elija entre las opciones disponibles. \n "
        read -n 1 -s -r -p "- Presione una tecla para continuar -"
        return;;
esac
}

# Funcion que elabora reporte de los 5 procesos con mayor consumo de memoria.
function reporte_cinco {
local lista5=$(ps ax -o %mem,command | sort -bnr -k3 | head -6 | tail +2 | awk '{print $1 ":" $2}') 
	cartel_report 5
	for proce in $lista5
      do
         porcentaje=$(echo $proce | cut -d: -f1)
         proceso=$(echo $proce | cut -d: -f2)
         echo ""
         echo "Proceso: $proceso"
         echo "Porcentaje de memoria utilizado: $porcentaje%"
         echo "-----------------------------------------------"
         
      done
}


# Funcion que muestra una alerta si se supera la cantidad de procesos por defecto (100)
# o el numero de procesos establecido por el ususario.
function alerta_proceso {
clear
numLimite=100
numProce=$(ps -aux --no-headers | wc -l)
echo "-----------------------------------------------------"
read -p "El número predeterminado de alerta es de 100 procesos 
¿Desea cambiar este valor?:  [ S / N ]: " resp
             case $resp in 
                       s|S) read -p "Ingrese un nuevo valor: " numLimite
                            if [[ $numProce -gt $numLimite ]]; then
                            cartel_report altP
                            echo -e "\nALERTA!!! La cantidad de procesos es de $numProce y SUPERA 
el valor establecido ($numLimite)."
                            echo "-----------------------------------------------------"
                            else 
                            cartel_report altP
                            echo -e "\nCALMA la cantidad de procesos es de $numProce y aún no supera 
el valor establecido ($numLimite)."
                            echo "-----------------------------------------------------"            
                            fi;;
                       n|N) echo -e "\nHa elegido continuar con el valor predeterminado (100)."
                            if [[ $numProce -gt $numLimite ]]; then
                            cartel_report altP
                            echo -e "\nALERTA!!! La cantidad de procesos es de $numProce y SUPERA 
el valor establecido ($numLimite)."
                            echo "-----------------------------------------------------"
                            else 
                            cartel_report altP
                            echo -e "\nCALMA la cantidad de procesos es de $numProce y aún no supera 
el valor establecido ($numLimite)."
                            echo "-----------------------------------------------------" 
                            fi;;
                         *) echo -e "\n Opcion invalida, seleccione S o N "
                            ;;
             esac
read -n 1 -s -r -p "- Presione una tecla para continuar -"
return
}


#Funcion que lista los procesos del usuario en curso, el ususario ingresa un PID a monitorear
#se toman 10 capturas de la ejecuccion de ese proceso con 2 segundos entre cada una.
function monitorPS {
   clear
   echo -e "\n   Listando todos los Procesos del Sistema: "
   echo "------------------------------------------------------"
   listaTotal=$(ps --no-headers -Ao pid | tr -d ' ')
   ps -Ao pid,%cpu,%mem,stat,comm
   echo "------------------------------------------------------"
   echo -e "\nA continuación se realizaran 10 capturas de la actividad del proceso elegido."
   echo ""
   read -p "Ingrese el PID del proceso a monitorear: " pidMon
   echo -e "\n------------------------------------------------------"
        if echo $listaTotal | grep -o $pidMon > /dev/null 2>&1; then
                 top -p $pidMon -b -n 10 -d 2 #modo no interactivo con 10 iteraciones y delay de 2 seg entre captura.

          else echo -e "\nERROR!!! Ingrese un PID válido";

         fi    
   echo -e "\n------------------------------------------------------"      
   read -n 1 -s -r -p "- Presione una tecla para continuar -"
return 
}


#Funcion que dado un PID procede a eliminarlo amablemente, de no lograrlo 
#intenta por la fuerza y en caso de no poder eliminarlo se emite una alerta
function PSkiller {
   clear
   echo -e "\n   Listando todos los Procesos del Sistema: "
   echo "------------------------------------------------------"
   listaTotalPS=$(ps --no-headers -Ao pid | tr -d ' ')
   ps -Ao pid,%cpu,%mem,stat,comm
   echo "------------------------------------------------------"
   read -p "Ingrese el PID del proceso a eliminar: " psRIP
     if echo $listaTotalPS | grep -o $psRIP > /dev/null 2>&1; then
         echo -e "\nProcediendo de forma segura..."
         if  kill -15 $psRIP > /dev/null 2>&1; then echo -e "\nProceso eliminado exitosamente!"

          elif [[ $? -eq 0 ]] || echo -e "\nFallo al eliminar proceso, forzando eliminacion.."; then 
          ( kill -9 $psRIP > /dev/null 2>&1  && echo -e "\nProceso eliminado exitosamente!" ) || 
            echo -e "\nDesafortunadamente no se pudo eliminar el proceso $psRIP."

         fi

       else echo -e "\nERROR!!! Ingrese un PID válido";

      fi
      echo "------------------------------------------------------"
      read -n 1 -s -r -p "- Presione una tecla para continuar -"
      return
}


#Funcion que recibe una lista de procesos y los elimina primero amablemente, luego fuerza la eliminacion
#y en ultima instancia emite un aviso.
function serialPSkiller {
   clear
   read -p "Ingrese la ruta del archivo CSV con el listado de PIDs : " listaPids
   if [[ -f $listaPids ]]; then 
      listaPIDS=$(cut -d "," -f 1 "$listaPids")
      listaTotalPIDS=$(ps --no-headers -Ao pid | tr -d ' ')
      for PiD in $listaPIDS; 
         do
          if (echo $listaTotalPIDS | grep -o $PiD > /dev/null 2>&1); then echo -e "\nProcediendo de forma segura..."
          ( kill -15 $PiD > /dev/null 2>&1 && echo -e "\nProceso $PiD eliminado exitosamente!" ) ||
          ( kill -9 $PiD > /dev/null 2>&1 && echo -e "\nProceso $PiD eliminado exitosamente!" ) || 
          echo -e "\nDesafortunadamente no se pudo eliminar el proceso $PiD."
          else echo -e "\nEl proceso con PID: $PiD no es un proceso válido."
          fi
      done

   fi 
  read -n 1 -s -r -p "- Presione una tecla para continuar -"
 return                

}



############################# FUNCIONES DE GESTION DE MEMORIA #############################

#Menu principal de gestion de memoria
function gestion_memoria {
        clear
	echo -e " \n****************************************************************
	                 GESTIÓN DE MEMORIA
**************************************************************** "
echo ""
echo " 1) Reporte del uso de Memoria RAM"
echo " 2) Alerta por uso de Memoria"
echo " 3) Volver al Menu Principal"
echo " 4) Ayuda"
echo " 5) Salir"
echo ""
read -p " Seleccione una opción: " optn
echo ""

case $optn in
     1) totalmem 
        ;;
     2) echo -e "\nEsta accion requiere un cliente de correos"
        verifica_pkg mail; #Se comprueba que este instalado el gestor de mails.
        alerta_memoria
        ;;
     3) return 
        ;;
     4) clear
        sed -n '263,+17p' biblioteca_toolkit.sh
        read -n 1 -s -r -p "- Presione una tecla para continuar -"
        ;;
     5) echo -e " \nHasta la Próxima\n "; exit 0 
        ;;
     *) echo -e " \n ERROR!!!, opción inválida, por favor elija entre las opciones disponibles. \n "
         read -n 1 -s -r -p "- Presione una tecla para continuar -"
         return
esac
}

#Funcion que genera reporte de la memoria ram del sistema.
function totalmem {
clear
ramUsada=$(free | awk '/Mem/{printf("%.2f\n"), $3/$2*100}') #imprime la operacion de memoria usada/memoria total*100 redondeando resultado y mostrando solo 2 decimales
totalRam=$(free -m | awk '/Mem/{print $2}')
totalSwap=$(free -m | awk '/Swa/{print $2}')
swapUsada=$(free | awk '/Swa/{printf("%.2f\n"), $3/$2*100}')
echo "------------------------------------------------------"
echo -e "\nEl Total de la Memoria RAM es de $totalRam mebibytes"
echo -e "\nEl total de la Memoria Swap es de $totalSwap mebibytes"
echo -e "\nLa cantidad de RAM actualmente en uso es del $ramUsada%"
echo -e "\nLa cantidad de Swap actualmente en uso es del $swapUsada%"
echo "-----------------------------------------------------"

read -n 1 -s -r -p "- Presione una tecla para continuar -"
return
}


###########################  FUNCIONES DE GESTION DE RED (TEXTO) ###################

#Menú de gestion de red.
function gestion_red {
 clear
 echo -e " \n****************************************************************
	                 GESTIÓN DE RED
**************************************************************** "
  echo ""
  echo " 1) Verificar Resolución de Hostname"
  echo " 2) Comprobar un Listado de DNS"
  echo " 3) Volver al Menu Principal"
  echo " 4) Ayuda"
  echo " 5) Salir"
  read -p " Seleccione una opción: " opt
  echo ""
	    case $opt in
	              1) resolucion_hostname
                    ;;
		           2) verificaDNS
                    ;;
		           3) return 
                    ;;
                 4) clear
                    sed -n '280,+15p' biblioteca_toolkit.sh
                    read -n 1 -s -r -p "- Presione una tecla para continuar -"
                    ;;
                 5) echo -e " \nHasta la Próxima\n "; exit 0 
                    ;;
		           *) echo -e " \n ERROR!!!, opción inválida, por favor elija entre las opciones disponibles. \n "
                    ;;
	    esac
   return
}


#Funcion que verifica que el nombre del host este asociado a la primera linea que coincide con la ip.
function resolucion_hostname {
   ( cat /etc/hosts | grep ^127.[0-9].[0-9].1 | grep $(hostname)  > /dev/null 2>&1 &&
   echo -e "\nEl nombre del host $(hostname) se encuentra configurado correctamente." ) ||
   echo -e "\n- ADVERTENCIA -  El nombre del host $(hostname) no se encuentra en la configuración. "
   read -n 1 -s -r -p "- Presione una tecla para continuar -"
   return
}

#Verifica que una lista de direcciones ip de servidores DNS se encuentren configurados en /etc/resolv.conf
#en caso contrario ofrece la opcion de agregarlos a la configuracion.
function verificaDNS {
       read -p "Ingrese un archivo con la lista de direcciones IP a comprobar: " listaDNS ;
       if [[ -f $listaDNS ]]; then 
            for dnsIP in `cat $listaDNS`; do 
               ( cat /etc/resolv.conf | grep -o $dnsIP > /dev/null 2>&1 ) && echo -e "\nEl servidor DNS ya se encuentra configurado correctamente." ||
                echo -e "\nEl servidor DNS $dnsIP no se encuentra en la configuración." 
                echo "Desea agregarlo?: [S / N] (requiere permisos de administrador)"; read SSNN
                 case $SSNN in
                       S|s) if verifica_root; then
                                read -p "Ingrese nombre para el servidor con IP $dnsIP: " serverName
                                sudo echo "$serverName $dnsIP" >> /tmp/DNS_prueba  #/etc/resolv.conf
                            fi
                       ;;     
                       N|n) echo -e "\n Ha elegido no agregarlo."
                       ;;
                         *) echo -e "\n ERROR!! opción inválida."
                       ;;
                 esac           

              done
        else echo -e "\n ERROR!! ruta o archivo inválidos."

       fi      
 read -n 1 -s -r -p "- Presione una tecla para continuar -"
 return           
}

########################  MENÚ PRINCIPAL Y OPCIONES #################

#Muestra todas las opciones de gestion
function menu_principal {
    echo "---------------------------------------------------------------------------
     ___________________________
    |\_________________________/|     
    ||                         ||    --- Bienvenido a Sysadmin Toolkit! ---
    ||    Sysadmin Toolkit     ||        La Herramienta que facilita la
    ||                         ||          Administración de Sistemas.
    ||        Ver 1.3          ||         
    ||                         ||         
    ||          2021           ||  
    ||_________________________|| 
    |/_________________________\|
      ___\_________________/___  __
    ┌──────────────────────────┐   )
    │┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼│   (_
    │┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼│    _|_
    │┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼┼│   | | |
    └──────────────────────────┘   |___|"
    echo "---------------------------------------------------------------------------"
 echo "1. Gestión de Usuarios."
 echo "2. Gestión de Discos."
 echo "3. Gestión de Procesos."
 echo "4. Gestión de Memoria."
 echo "5. Gestión de Red."
 echo "6. Ayuda."
 echo "7. Salir."
}

#Menú de entrada para la iteracción entre las gestiones del menú y admin
function entrada_menu {
 read -p "  ¿Qué desea gestionar?: " opcion
 case $opcion in
  1) gestion_usuarios ;;
  2) gestion_discos ;;
  3) gestion_procesos ;;
  4) gestion_memoria ;;
  5) gestion_red ;;
  6) ayuda | more -d ;;
  7) clear; echo -e " \nHasta la Próxima\n "; exit 0 ;;
  *) echo -e " \n ERROR!!!, selección inválida, por favor elija entre las opciones disponibles. \n"
     read -n 1 -s -r -p "- Presione una tecla para continuar -"
     return
 esac
}


##########################  -  BUCLE  PROGRAMA  PRINCIPAL  - ##########################

while true 
  do 
  clear
  menu_principal
  entrada_menu
done



