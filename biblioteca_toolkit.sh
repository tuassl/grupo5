#!bin/bash

#--------------------------------------------------------------------------------#
# Este archivo es una Biblioteca de Utilidades y funciones para Sysadmin Toolkit #
#--------------------------------------------------------------------------------#
#                      Universidad Nacional del Comahue                          #
#                           Facultad de Informática                              #
#                                  Grupo 5                                       #
#                                                                                #
#                      Fernando Andrés Pinilla Álvarez                           #
#--------------------------------------------------------------------------------#


##################################################################################


#Funcion para comprobar si el user tiene permisos de root
#Requisito para eliminar y añadir usuarios, utilizar iotop, editar lista de DNS.
function verifica_root () {
    if ! [[ $(id -u) = 0 ]]; then
    echo -e "\n Usted no tiene permisos suficientes, ejecute el programa como administrador. \n"
    exit 
    fi
}

#Comprueba que el parametro dado ocmo argumento sea un entero.
function verifica_entero {
 ReTorno=1     
 if [[ $1 =~ ^?[0-9]+$ ]];
    then
    ReTorno=0
    return $ReTorno
 fi
}



#funcion para verificar si se encuentra instalado el paquete
function verifica_pkg {
ruta=$( command -v $1 )
which $1 1>/dev/null
valSal=$?
       if [ $valSal -eq 0 ]; then
         echo -e "\n El programa solicitado ya se encuentra instalado en $ruta.\n"
         apt-cache policy $1 2>/dev/null;
         else read -p "El paquete solicitado no se encuentra instalado. Desea descargarlo? [S / N]" choice
              case $choice in
              S|s) echo -e " \n -- Por favor espere mientras finaliza el procedimiento --\n"
                   echo -e "\n -- Actualizando lista de Paquetes --"; apt update
                   echo -e "\n -- Actualizando los Paquetes Disponibles --" ; apt upgrade
                   echo -e "\n -- Finalizando Instalacion -- "; apt install $1

                   echo -e "\n-- Instalación Finalizada --";;

              N|n) echo -e "\nHa elegido no instalar el paquete. ";;
                *) echo -e "\nOpción inválida";;
              esac
       fi
}

function genera_pass {
	randomPass=$(date +%s | sha256sum | base64 | head -c 12)
	echo "$randomPass"
}

function verifica_user {
       reT=1
       local user="$1"
       if  getent passwd | grep ^"$user" ; then
           reT=0
           echo "$reT"
       fi
}

#Funcion con distintos carteles para imprimir en el programa principal.
function cartel_report () {
        if [[ $1 == reg ]]; then 
             echo -e " \n****************************************************************
	         REPORTE DE USUARIOS REGULARES
**************************************************************** "
      elif [[ $1 == sys ]]; then 
             echo -e " \n****************************************************************
	         REPORTE DE USUARIOS DE SISTEMA
**************************************************************** "
      elif [[ $1 == io ]]; then
       echo -e " \n****************************************************************
	         REPORTE DE ACTIVIDAD INPUT/OUTPUT
**************************************************************** "
      elif [[ $1 == dl ]]; then
       echo -e " \n****************************************************************
	         REPORTE DE RENDIMIENTO DE DISCOS LOCALES
**************************************************************** "
      elif [[ $1 == 5 ]]; then
       echo -e " \n****************************************************************
      REPORTE DE LOS 5 PROCESOS CON MAYOR CONSUMO DE MEMORIA
**************************************************************** "
      elif [[ $1 == altP ]]; then
       echo -e " \n******************************************************
           ALERTA SEGUN CANTIDAD DE PROCESOS
****************************************************** "
       fi
}

 #Envia una alerta y un reporte al mailbox local si se supera el 50% de uso de RAM o el valor establecido por el usuario.
function alerta_memoria {
  declare -i ret
  declare -i valorLimite=50
  ramEnUso=$(free | awk '/Mem/{printf("%.2f\n"), $3/$2*100}' | cut -d "," -f1) #Imprime memoria usada/memoria total*100 redondeando sin mostrar decimales
  read -p "El valor a superar por defecto para que se envíe una alerta es del 50%. Desea cambiar ese valor?: [S / N] " resP
  case $resP in
          S|s) read -p "Ingrese el nuevo límite: " valorLimite
               ret=$(verifica_entero $valorLimite)
               if [[ $ret -ne 0 ]]; then echo "ERROR!! ingrese un número válido."
               fi;;
          N|n) echo "Ha elegido continuar con el valor predeterminado $valorLimite" ;;

            *) echo "ERROR!!! Opción inválida." ;;
  esac

      if [[ $ramEnUso -gt $valorLimite ]]; then
             temporal=$(mktemp) #al asignar ya se crea el archivo
             ASUNTO="ALERTA: El uso de memoria RAM es alto en $(hostname) el $(date)"
             reporte="$temporal"
             PARA="$USER"
             echo "------------------------------------------------------------------" >> $reporte
             echo "La cantidad de memoria en uso es del: $ramEnUso%" >> "$reporte"
             echo "" >> $reporte
             echo "------------------------------------------------------------------" >> $reporte
             echo "Consumo de memoria con información de TOP" >> $reporte
             echo "------------------------------------------------------------------" >> $reporte
             echo "$(top -b -o +%MEM | head -n 20)" >> $reporte
             echo "" >> $reporte
             echo "------------------------------------------------------------------" >> $reporte
             listaProce=$(reporte_cinco); echo "$listaProce" >> $reporte
             mail -s "$ASUNTO" "$PARA" < "$reporte"
             echo -e "\nATENCION: El uso de memoria SUPERA el límite establecido"
             echo "Se genero un reporte del uso de RAM y fue enviado a su correo."
     else
             echo -e "\nCALMA: El uso de memoria RAM esta por debajo del $valorLimite%, no es necesario enviar alerta. "
     fi
 
 read -n 1 -s -r -p "- Presione una tecla para continuar -"

 return
}


# Función de ayuda general del menú principal.
function ayuda() {
       clear
	echo -e " \n****************************************************************************
	                           AYUDA
**************************************************************************** "
    echo -e " \n Esta herramienta es un conjunto de scripts con un menú interactivo en shell.
 Cada opcion del menú tiene submenus para ejecutar las tareas solicitadas.
-----------------------------------------------------------------------------
 Para asegurar un correcto funcionamiento al momento de proporcionar archivos 
 CSV (Coma Separated Value) verifique que tengan el siguiente formato:

nombre1,
nombre2,
nombre3,

-----------------------------------------------------------------------------
---------------------------  Gestión de Usuarios  ---------------------------
-----------------------------------------------------------------------------


* Alta de Usuarios: (Administrador)
Dado un nombre de usuario o un archivo de tipo CSV con un listado
crea el/los usuarios en el sistema con una password generica y su 
respectivo directorio Home.


* Cruce de Claves: (Administrador)
Dado como parámetro un listado de usuarios en archivo de tipo CSV
se generan e intercambian llaves SSH entre el host que ejecuta el 
script y los equipos en la lista.


* Baja de Usuarios: (Administrador)
Dado un nombre de usuario o un archivo de tipo CSV con un listado de
nombres elimina el/los usuarios en el sistema junto con su directorio 
personal y su cola de correos.


* Reporte de Usuarios Regulares:
Dado una direccion IP o un listado de direcciones en archivo de tipo 
CSV se elabora un reporte por pantalla con el nombre y la cantidad 
de usuarios regulares.


* Reporte de Usuarios del Sistema:
Dado una direccion IP o un listado de direcciones en un archivo CSV
se elabora un reporte por pantalla con el nombre y la cantidad de 
usuarios del sistema.



-----------------------------------------------------------------------------
---------------------------  Gestión de Discos  -----------------------------
-----------------------------------------------------------------------------


* Reporte de Espacio en Disco:
Dado una direccion IP o un listado de direcciones en un archivo de tipo 
CSV, se elabora un informe de los discos para cada equipo.
El informe reporta: partición, tamaño total, % utilizado, % disponible, 
punto de montaje y sistema de archivos.


* Reporte de Rendimiento de Discos Locales: (Administrador)
Se elabora un informe del rendimiento de los discos locales con el paquete
de utilidades sysstat. En el caso de que la herramienta no esté instalada
se ofrece la posilibilidad de descarga e instalación.


* Reporte de Actividad de I/O en Discos Primarios: (Administrador)
Dado una direccion IP o un listado de direcciones en un archivo de tipo CSV
se elabora un informe de actividad de Entrada y Salida de los discos 
primarios para cada equipo, utilizando la herramienta iotop.
En el caso de no estar instalado dicho paquete, se ofrece la posibilidad de
descarga e instalación.



-----------------------------------------------------------------------------
---------------------------  Gestión de Procesos  ---------------------------
-----------------------------------------------------------------------------


* Reporte de los Primeros 5 Procesos por consumo de Memoria: (Localmente)
Se elabora un reporte de manera local de los primeros 5 procesos ordenados
por uso de memoria.(De mayor a menor consumo.)


* Reporte de alerta: (Localmente)
Se envía un reporte en forma de  alerta al correo preestablecido en caso 
de que el número de procesos locales sea mayor al umbral provisto por el 
usuario.


* Monitorear Proceso Específico: (Localmente)
Dado un identificador de proceso (PID) se realiza un seguimiento del mismo
con una iteración por pantalla de 10 veces.


* Eliminar un Proceso Específico: (Localmente)
Dado un identificador de proceso (PID) se procede a eliminarlo de manera
amable, en caso de no responder se fuerza la eliminación del mismo, y en
ultima instancia si no se puede realizar ninguna de las acciónes anteriores
se emite una advertencia.


 * Eliminar una lista de Procesos: (Localmente)
Dado un archivo de tipo CSV con un listado de identificadores de procesos 
(PID) se procede a eliminarlos de manera amable, en caso de no responder 
se fuerza la eliminación de los mismos, y en ultima instancia si no se puede 
realizar ninguna de las acciónes anteriores se emite una advertencia.



-----------------------------------------------------------------------------
---------------------------  Gestión de Memoria  ----------------------------
-----------------------------------------------------------------------------

* Reporte del uso de Memoria RAM: (Localmente)
Se labora un reporte del uso de la Memoria RAM, en el caso de superar un uso
del 20% se genera un archivo temporal que se enviará con una alerta al mailbox
local.


*Alerta por uso de Memoria:
Se configura un reporte del uso de memoria que será almacenado en forma de log
y se enviará al mail local en caso de superar el 50% de uso de RAM. O el valor 
provisto por el usuario. 



-----------------------------------------------------------------------------
-----------------------------  Gestión de Red  ------------------------------
-----------------------------------------------------------------------------

* Verificar resolución de Hostname: (Localmente)
Se verifica que la resolución del hostname local  se encuentra correctamente
configurado en la base de datos de host (/etc/hosts). En caso de no estar bien
seteado se mostrará una advertencia.


* Obtener Listado de DNS: (Administrador)
Se muestra un listado de servidores DNS y se verifica que los mismos estén
correctamente configurados, de no ser así serán agregados a la configuración
actual.


\n****************************************************************************
	                           AYUDA
****************************************************************************
"
}


#Se exportan las funciones
export -f ayuda
export -f genera_pass
export -f verifica_user
export -f verifica_root
export -f verifica_entero
export -f verifica_pkg
export -f cartel_report
export -f alerta_memoria

