#!/bin/bash

#Este script crea 10 instancias de 5 minutos del proceso sleep y guarda los PID en un archivo  
#con el proposito de alimentar la funcionalidad del script sysadmin_toolkit.sh para eliminar 
#una lista de procesos. Se pueden terminar todas las instancias en cualquier momento al pulsar ENTER. 
for num in {1..10}; do
    sleep 300 &
    echo "Instancia $num creada.."
done

ps -A | grep sleep | head -10 | awk '{print $1}' > listado10PS.txt
read -p "- Presione [ENTER] para terminar todas las instancias -"
killall sleep >/dev/null 2>&1
