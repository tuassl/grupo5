# Grupo 5 
---------
### Alumno:
- Fernando Andrés Pinilla Álvarez

#### Branch evaluada: master

#Etapas del proyecto

###Menu: 100%
-----
OK!

###Usuarios: 100%
--------
OK!

###Discos: 100%
-------
OK!

###PROCESOS: 100%
---------
OK!

###MEMORIA: 95%
--------
OK!
1) Pequeño detalle al instalar el cliente de correos, si la instalación falla el proceso continúa y luego da un error. Sugerencia: Volver a verificar que el cliente de correo
esta instalado correctamente, en caso negativo no volver a preguntar y decirle al usuario que debe instalar un cliente de correos, para evitar errores inesperados.
(No voy a restar puntos por esto)

###TEXTO: 100%
------
OK!

#Devolución general:
-------------------
El proyecto está muy bien!. Los menú, la documentación y el código son correctos y satisfacen el objetivo de este trabajo. 
Como recomendación separar el código del sysadmin_toolkit hubiese sido interesante para no tener un archivo taaan grande para mantener y eventualmente compartir.
Es un TP difícil para trabajarlo solo, con lo cual es sin lugar a dudas un gran logro de tu parte.

Puntuación general del trabajo <b>10 pts.</b> Felicitaciones!


